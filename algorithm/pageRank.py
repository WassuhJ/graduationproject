# -*- coding = utf-8 -*-
# @Time: 2024/3/21 16:37
# @File: pageRank.py
# @Software: PyCharm

import networkx as nx
import pandas as pd

# 计算图中节点的PageRank
def page_rank(G, addr2idx):
    pagerank = nx.pagerank(G, alpha=0.85)

    pagerank_df = pd.DataFrame(list(pagerank.items()), columns=['idx', 'pagerank'])
    addr2idx = pd.merge(addr2idx, pagerank_df, on='idx')

    sorted_addr2idx = addr2idx.sort_values(by='pagerank', ascending=False)
    print(sorted_addr2idx)