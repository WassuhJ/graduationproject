﻿1.English Version
2.中文版

============================================
Ethereum partial transaction dataset
============================================
By InPlusLab, Sun Yat-sen University


================== References ==================

• Website
	http://xblock.pro/ethereum/

• Cite
	@article{ lin2020modeling,
	  title={Modeling and Understanding Ethereum Transaction Records via A Complex Network Approach},
	  author={Dan Lin, Jiajing Wu, Qi Yuan, Zibin Zheng},
	  journal={IEEE Transactions on Circuits and Systems--II: Express Briefs },
	  year={2020},
	  month={to be published},
	  publisher={IEEE},
	  doi={10.1109/TCSII.2020.2968376}
	}


================== Change Log =================

• Version 1.0, released on 04/12/2019


=================== Source ====================

This dataset is crawled from https://etherscan.io/ using API.


================ File Information =================


• Description
	The task of link prediction aims to predict the occurrence of links in a given graph on the basis of observed information.
	In the temporal link prediction problem, unlike the static link prediction where links have no timestamp,
	we use the existing links in the past (with smaller timestamps) as the training data to predict the occurrences of links in the future (with larger timestamps).


• Contents
	- EthereumG1
		Ethereum Transaction Network. It is centered by the account with K-in = K-out = 3.
		Size: 21.6 MB
	- EthereumG2
		Ethereum Transaction Network. It is centered by the account with K-in = K-out = 4. 
		Size: 24.1 MB
	- EthereumG3
		Ethereum Transaction Network. It is centered by the account with K-in = K-out = 5. 
	 	Size: 69.5 MB

* Notes * 
	The file structure of EthereumG2 and EthereumG3 please refer to EthereumG1. 



———————————— EthereumG1 ————————————

• addr2Idx.txt
	- Row:
		Each row represents a mapping from Ethereum address to unique node number (ID).
		Sum: 9,855 rows
  
	- Column:
		1. addr : Ethereum address 
		2.  idx : Node number (ID)
		Sum: 2 columns

• LPsubG1_df_sort.pickle 
	Sort all the collected edges according to their timestamps. Load this pickle file as DataFrame ['From', 'To', 'Value','TimeStamp' ]

	- Column:
		1.     Value : The amount of money transferred
		2. Timestamp : When the transaction happens
		3.      From : The sender of the transaction
		4.        To : The recipient of the transaction

• LPsubG1_0.5_TransEdgelist.txt

	- Row:
		Each row represents an edge information.
		Sum: 225,714 rows
  
	- Column:
		1. from_node_num : Node number (From) 
		2.   to_node_num : Node number (To)
		3.         value : Value
		4.     timestamp : Timestamp
		Sum: 4 columns

• LPsubG1_train_test_split_0.5.pickle
	Dataset split. Load this pickle file as a dict.
	- Elements:
		1.   train_test_split['train_edges_pos'] : positive node pairs in training set
		2.    train_test_split['test_edges_pos'] : positive node pairs in test set
		3. train_test_split['train_edges_false'] : negative node pairs in training set
		4.  train_test_split['test_edges_false'] : negative node pairs in test set


* Notes * 

	1. (addr2Idx.txt) Node numbers are unique identifiers labelling from number 1. 


===================Contact====================

Please contact Dan Lin (lind8@mail2.sysu.edu.cn) for any questions about the dataset.



============================================
以太坊局部交易数据集
============================================
By InPlusLab, 中山大学


==================== 参考 =====================

• 相关网站
	http://xblock.pro/ethereum/

• 引用
    @article{ lin2020modeling,
	  title={Modeling and Understanding Ethereum Transaction Records via A Complex Network Approach},
	  author={Dan Lin, Jiajing Wu, Qi Yuan, Zibin Zheng},
	  journal={IEEE Transactions on Circuits and Systems--II: Express Briefs },
	  year={2020},
	  month={to be published},
	  publisher={IEEE},
	  doi={10.1109/TCSII.2020.2968376}
	}


=================== 变更日志 ====================

• 版本 1.0, 发布于 04/12/2019


=================== 数据来源 ====================

使用网站（https://etherscan.io/）提供的API工具爬取的交易数据。


=================== 文件信息 ====================


• 描述
	链路预测任务旨在根据观察到的信息预测给定图中链路的出现。
	在时序链路预测问题中，与链路没有时间戳的静态链路预测不同，
	我们将过去已存在的链路（带有较小的时间戳）用作训练数据，以预测将来的链路（带有较大的时间戳）的出现。


• 内容
	- EthereumG1
		以 K-in = K-out = 3 的帐户为中心的以太坊交易网络。
		大小: 21.6 MB
	- EthereumG2
		以 K-in = K-out = 4 的帐户为中心的以太坊交易网络。
		大小: 24.1 MB
	- EthereumG3
		以 K-in = K-out = 5 的帐户为中心的以太坊交易网络。
	 	大小: 69.5 MB

* 注释 * 
	EthereumG2 和 EthereumG3 的文件结构说明请参考 EthereumG1。



———————————— EthereumG1 ———————————— 

• addr2Idx.txt
	- 行:
		每一行表示把以太坊的账户（地址）映射为一个唯一的编号（ID）
		总和：9,855行
  
	- 列:
		1. addr : 以太坊的账户（地址）
		2.  idx : 编号（ID）
		总和: 2列

• LPsubG1_df_sort.pickle 
	根据所有爬取的交易的交易时间进行排序。
	加载这个pickle文件后得到一个DataFrame。（['From', 'To', 'Value','TimeStamp']

	- 列:
		1.     Value : 交易金额
		2. Timestamp : 交易时间戳
		3.      From : 交易发送方
		4.        To : 交易接收方


• LPsubG1_0.5_TransEdgelist.txt

	- 行:
		每一行代表了一条边信息。
		总和: 225,714行
  
	- 列:
		1. from_node_num : 边的起始节点序号
		2.   to_node_num : 边的到达节点序号
		3.         value : 交易额
		4.     timestamp : 时间戳
		总和: 4 列

• LPsubG1_train_test_split_0.5.pickle
	数据集划分。
	加载这个pickle文件后得到一个set类型。
	- 元素:
		1.   train_test_split['train_edges_pos'] : 训练集中的正样本（边）元组
		2.    train_test_split['test_edges_pos'] : 测试集中的正样本（边）元组
		3. train_test_split['train_edges_false'] : 训练集中的负样本（边）元组
		4.  train_test_split['test_edges_false'] : 测试集中的负样本（边）元组


* 注释 * 

	1. (addr2Idx.txt) 节点序号是从数字1开始编号的唯一标识符。


==================== 联系 =====================

若对数据集有任何问题请联系 林丹 (lind8@mail2.sysu.edu.cn)。