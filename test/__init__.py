# import torch
#
# print(torch.__version__)                # 查看pytorch安装的版本号
# print(torch.cuda.is_available())        # 查看cuda是否可用。True为可用，即是gpu版本pytorch
# print(torch.cuda.get_device_name(0))    # 返回GPU型号
# print(torch.cuda.device_count())        # 返回可以用的cuda（GPU）数量，0代表一个
# print(torch.version.cuda)               # 查看cuda的版本

import networkx as nx
import community
import matplotlib.pyplot as plt

# 创建一个无向图
G = nx.Graph()

# 添加节点（用户）
G.add_node(1)
G.add_node(2)
G.add_node(3)
G.add_node(4)
G.add_node(5)
G.add_node(6)
G.add_node(7)
G.add_node(8)
G.add_node(9)
G.add_node(10)

# 添加边（连接关系）
G.add_edge(1,2)
G.add_edge(1,3)
G.add_edge(1,4)
G.add_edge(2,3)
G.add_edge(2,4)
G.add_edge(3,4)
G.add_edge(5,6)
G.add_edge(5,7)
G.add_edge(6,7)
G.add_edge(8,9)
G.add_edge(8,10)
G.add_edge(9,10)

# 使用Louvain算法进行社区发现
partition = community.best_partition(G)

# 绘制社区结构图
size = float(len(set(partition.values())))
pos = nx.spring_layout(G)
count =0
for com in set(partition.values()):
    count +=1
    list_nodes = [nodes for nodes in partition.keys() if partition[nodes] == com]
    nx.draw_networkx_nodes(G, pos, list_nodes, node_size=20, node_color=f'C{count}')
nx.draw_networkx_edges(G, pos, alpha=0.5)
plt.show()