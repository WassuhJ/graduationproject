# -*- coding = utf-8 -*-
# @Time: 2024/3/21 13:08
# @File: main.py
# @Software: PyCharm

from model import dataloader, graph
from algorithm import pageRank

if __name__ == '__main__':
    # 创建一个有向图
    addr2idx, edge_list = dataloader.load_net_data()
    G = graph.createGraph(addr2idx, edge_list)

    pageRank.page_rank(G, addr2idx)

