# -*- coding = utf-8 -*-
# @Time: 2024/3/21 13:51
# @File: graph.py
# @Software: PyCharm

import networkx as nx

def createGraph(addr2idx, edge_list):
    G = nx.DiGraph()
    for idx in addr2idx['idx']:
        G.add_node(idx)
    for _, row in edge_list.iterrows():
        G.add_edge(row['from_node_num'], row['to_node_num'], weight=row['value'])
    # print(nx.info(G))

    # nx.draw(G, with_labels=True, edge_color='b', node_color='g', node_size=1000)
    # plt.show()
    # plt.savefig('./generated_image.png') 如果你想保存图片，去除这句的注释
    return G