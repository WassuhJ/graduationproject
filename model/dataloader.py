# -*- coding = utf-8 -*-
# @Time: 2024/3/21 16:40
# @File: dataloader.py
# @Software: PyCharm

import pandas as pd

addr2idx_path = './input/ethereum-partial-transaction-dataset/EthereumG1/addr2Idx.txt'
edge_list_path = './input/ethereum-partial-transaction-dataset/EthereumG1/LPsubG1_0.5_TransEdgelist.txt'
tx_sort_path = './input/ethereum-partial-transaction-dataset/EthereumG1/LPsubG1_df_sort.pickle'
train_test_split_path = './input/ethereum-partial-transaction-dataset/EthereumG1/LPsubG1_train_test_split_0.5.pickle'


def load_net_data():
    # 每一行表示把以太坊的账户（地址）映射为一个唯一的编号（ID）
    addr2idx = pd.read_csv(addr2idx_path, sep=',', header=None, names=['addr', 'idx'])
    # 每一行代表了一条边信息
    edge_list = pd.read_csv(edge_list_path, sep=',', header=None
                            , names=['from_node_num', 'to_node_num', 'value', 'timestamp'])
    return addr2idx, edge_list
